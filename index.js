//2 
db.fruits.count(
	{onSale: true}
	);

// 3.
db.fruits.count(
	{stocks: {$gt: 20}}
	);

// 4.
db.fruits.aggregate(
	[
		{ $match: {onSale: true}},
		{ $group: {_id: "$supplier_id", AvePrice: { $avg: "$price"} }}
	]
);

// 5.
db.fruits.aggregate(
	[
		{ $group: {_id: "$supplier_id", maxPrice: { $max: "$price"} }}
	]
);

// 6. 
db.fruits.aggregate(
	[
		{ $group: { _id:"$supplier_id", minPrice:{$min: "$price"} } } 
	]
);